$('.team-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
    centerMode: true,
    arrows: false,
    dots: false,
    responsive: [
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1
            }
        }
    ]
});

function openFile(){
    event.stopPropagation()
    var file = $(event.currentTarget).next()
    file.click()
    file.change((p)=>{
        alert("file selected")
        console.log(file.val())
    })
}

function openDropdown(){
    $($(event.currentTarget).parent()).toggleClass('active')
}